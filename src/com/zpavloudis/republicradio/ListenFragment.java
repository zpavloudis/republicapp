package com.zpavloudis.republicradio;

import android.app.Activity;
import android.app.ActivityManager;
import android.app.ActivityManager.RunningServiceInfo;
import android.app.ProgressDialog;
import android.content.ComponentName;
import android.content.Context;
import android.content.DialogInterface;
import android.content.DialogInterface.OnCancelListener;
import android.content.Intent;
import android.content.ServiceConnection;
import android.graphics.Typeface;
import android.os.Bundle;
import android.os.IBinder;
import android.os.SystemClock;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.Chronometer;
import android.widget.ImageButton;
import android.widget.TextView;
import android.widget.ToggleButton;

import com.zpavloudis.republicradio.PlayerService;
import com.zpavloudis.republicradio.IMediaPlayerServiceClient;
import com.zpavloudis.republicradio.StatefulMediaPlayer;
import com.zpavloudis.republicradio.PlayerService.MediaPlayerBinder;

public class ListenFragment extends Fragment implements IMediaPlayerServiceClient{
	
    private PlayerService mBoundService;
    private StatefulMediaPlayer mMediaPlayer;
    private boolean mBound;
    
    private ProgressDialog mProgressDialog;
    private String tag = "ListenFragment";
    private Chronometer playingTimer;
    
    //////////////////////////////////////////////////////
    private static final String KEY_CONTENT = "Fragment:Listen";
    public static ListenFragment newInstance(String content) {
    	ListenFragment fragment = new ListenFragment();

        fragment.mContent = content;

        return fragment;
    }

    private String mContent = "???";

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        if ((savedInstanceState != null) && savedInstanceState.containsKey(KEY_CONTENT)) {
            mContent = savedInstanceState.getString(KEY_CONTENT);
        }
    }///////////////////////////////////////////////////////

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
    	Log.d(tag, "onCreateView(): inflate layout,initialize button listeners");
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.listen_page, container, false);
        
        //bind to service
        doBindService();
    
        /**
        //Initialise buttons
        ImageButton playButton = (ImageButton) view.findViewById(R.id.playButton);
        playButton.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(final View v) {
                play(v);
            }
        });
        ImageButton pauseButton = (ImageButton) view.findViewById(R.id.pauseButton);
        pauseButton.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(final View v) {
                pause(v);
            }
        }); **/
        
        //set now playing font
        TextView tv=(TextView)view.findViewById(R.id.nowPlayingTextView);
        Typeface face=Typeface.createFromAsset(tv.getContext().getAssets(),
                                              "fonts/Milonga-Regular.ttf");
        tv.setTypeface(face,0);
        
        //PLAY/PAUSE BUTTON
       
        final ToggleButton playPauseButton = (ToggleButton) view.findViewById(R.id.playPauseButton);
        final Chronometer playingTimer = (Chronometer) view.findViewById(R.id.chronometer1);
        playPauseButton.setOnClickListener(new OnClickListener() {
 
            @Override
            public void onClick(final View v) {
                if (mBound) {
                    mMediaPlayer = mBoundService.getMediaPlayer();
 
                    //pressed pause ->pause
                    if (!playPauseButton.isChecked()) {
 
                        if (mMediaPlayer.isStarted()) {
                        	mBoundService.pause();
                            
                        	playingTimer.stop();
                        }
 
                    }
 
                    //pressed play
                    else if (playPauseButton.isChecked()) {
                        // STOPPED, CREATED, EMPTY, -> initialize
                        if (mMediaPlayer.isStopped()
                                || mMediaPlayer.isCreated()
                                || mMediaPlayer.isEmpty())
                        {
                        	mBoundService.initialisePlayer();//mSelectedStream);
                        }
 
                        //prepared, paused -> resume play
                        else if (mMediaPlayer.isPrepared()
                                || mMediaPlayer.isPaused())
                        {
                        	mBoundService.play();
                        	playingTimer.start();
                        }
 
                    }
                }
            }
 
        });
        
        //invisible stop button
        final ImageButton stopButton = (ImageButton) view.findViewById(R.id.stopButton);
        stopButton.setOnClickListener(new OnClickListener() {
        	
            @Override
            public void onClick(final View v) {
            	Log.d(tag,"in stop() event");

            	if(!mBound) {
            		doBindService();
            	}
            	
        		//return statefull media player.
                mMediaPlayer = mBoundService.getMediaPlayer();
                if( mMediaPlayer != null){
        		if( mMediaPlayer.isPaused() || mMediaPlayer.isStarted() ) {
            		mBoundService.stop();
            		doUnbindService();
            		getActivity().stopService(new Intent(getActivity(), PlayerService.class));
        		} }
            }
        });

        //returns view
        return view;
    }

    public void doBindService() {
    	Log.d(tag,"check PlayerService, bind to it");
        // Establish a connection with the service.  We use an explicit
        // class name because we want a specific service implementation that
        // we know will be running in our own process (and thus won't be
        // supporting component replacement by other applications).
       
        if (PlayerServiceRunning()) {
            // Bind to LocalService
            getActivity().bindService(new Intent(getActivity(), 
                    PlayerService.class), mConnection, Context.BIND_AUTO_CREATE);
            
        }
        else {
        	getActivity().startService(new Intent(getActivity(),PlayerService.class));
            getActivity().bindService(new Intent(getActivity(), 
                    PlayerService.class), mConnection, Context.BIND_AUTO_CREATE);
         }
    }
    
    /** Determines if the MediaPlayerService is already running.
     * @return true if the service is running, false otherwise.
     */
    private boolean PlayerServiceRunning() {
 
        ActivityManager manager = (ActivityManager) getActivity().getSystemService(Context.ACTIVITY_SERVICE);
 
        for (RunningServiceInfo service : manager.getRunningServices(Integer.MAX_VALUE)) {
            if ("com.zpavloudis.republicradio.PlayerService".equals(service.service.getClassName())) {
                return true;
            }
        }
 
        return false;
    }
 
    public void onInitialisePlayerSuccess() {
    	//mProgressDialog.dismiss();
    	
    	final TextView textView = (TextView) this.getActivity().findViewById(R.id.textView1);
    	textView.setText("Done!, playing Republic 100,3");
        final ToggleButton playPauseButton = (ToggleButton) this.getActivity().findViewById(R.id.playPauseButton);
        playPauseButton.setChecked(true);
        final Chronometer playingTimer = (Chronometer) this.getActivity().findViewById(R.id.chronometer1);
        playingTimer.setBase(SystemClock.elapsedRealtime());
        playingTimer.start();
    }
    
    public void onInitialisePlayerStart(String message) {
    /**    mProgressDialog = ProgressDialog.show(getActivity(), "", message, true);
        mProgressDialog.setCancelable(true);
        mProgressDialog.setOnCancelListener(new OnCancelListener() {
 
            @Override
            public void onCancel(DialogInterface dialogInterface) {
            		mBoundService.reset();
                  //  final ToggleButton playPauseButton = (ToggleButton) getActivity().findViewById(R.id.playPauseButton);
                  // playPauseButton.setChecked(false);
            }
 
        }); **/
    	final TextView textView = (TextView) this.getActivity().findViewById(R.id.textView1);
    	textView.setText(message);
 
    }
    /***
    public void play(View v) {
    	Log.d(tag,"in play() event"); 

    	//if service's not bounded, then bound it maafakaaa
    	if(!mBound) {
    		doBindService();
    	}
		
    	//return statefull media player.
        mMediaPlayer = mBoundService.getMediaPlayer();
    		//if player is null/stopped/empty whatever then initialise and prepare it
    		//it'll play automatically onPrepare
    		if( mMediaPlayer.isStopped()
    				|| mMediaPlayer.isCreated()
    				|| mMediaPlayer.isEmpty() ) {

    			mBoundService.initialisePlayer(); //initialize player.
    		}
    		//else, if player is paused, then resume it
    		else if( mMediaPlayer.isPrepared() 
    				|| mMediaPlayer.isPaused() ) {
	    	
    			mBoundService.play(); 
    		}
    	}

    
    public void pause(View v) {
    	Log.d(tag, "in pause() event");
    	
    	if(!mBound) {
    		doBindService();
    	}
    	
		//return statefull media player.
        mMediaPlayer = mBoundService.getMediaPlayer();
    	//if playing then pause it
		if( mMediaPlayer.isStarted() ) {
			mBoundService.pause();
		}
    }
     ***/

     //IMediaPlayerServiceClient
     @Override
     public void onError() {
             mProgressDialog.cancel();
     }

    private ServiceConnection mConnection = new ServiceConnection() {
    	
        public void onServiceConnected(ComponentName className, IBinder service) {
            //bound with Service. get Service instance
            mBoundService = ((MediaPlayerBinder)service).getService();
            
            //send this instance to the service, so it can make callbacks on this instance as a client
            mBoundService.setClient(ListenFragment.this);
            mBound = true;
            
            //Set play/pause button to reflect state of the service's contained player
            final ToggleButton playPauseButton = (ToggleButton) getView().findViewById(R.id.playPauseButton);
            playPauseButton.setChecked(mBoundService.getMediaPlayer().isPlaying());
 

        }

        public void onServiceDisconnected(ComponentName className) {
            // This is called when the connection with the service has been
            // unexpectedly disconnected -- that is, its process crashed.
            // Because it is running in our same process, we should never
            // see this happen.
            mBoundService = null;

        }
    };

    void doUnbindService() {
        if (mBound) {
            // Detach our existing connection.
        	getActivity().unbindService(mConnection);
        	mBound = false;
        }
    }
    public static final String EXTRA_TITLE = "title";
    public static Bundle createBundle( String title ) {
        Bundle bundle = new Bundle();
        bundle.putString( EXTRA_TITLE, title );
        return bundle;
    }
    
    public void onDestroy() {
        /**
         * Closes unbinds from service, ??stops the service??.
         */
    	//mBoundService.stop(); //stop/release media player
    	doUnbindService(); //unbind from playerservice
        //getActivity().stopService(new Intent(getActivity(), PlayerService.class));
        super.onDestroy();
        }
}

