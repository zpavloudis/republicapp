package com.zpavloudis.republicradio;

import android.content.Context;
import android.content.Intent;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.Bundle;
import android.support.v4.view.ViewPager;
import android.util.Log;
import android.widget.ImageButton;
import android.widget.Toast;

import com.actionbarsherlock.app.SherlockFragmentActivity;
import com.actionbarsherlock.view.Menu;
import com.actionbarsherlock.view.MenuInflater;
import com.actionbarsherlock.view.MenuItem;
/*import com.viewpagerindicator.TitlePageIndicator;
import com.viewpagerindicator.TitlePageIndicator.IndicatorStyle;*/

import com.zpavloudis.republicradio.FragmentAdapter;

public class MainActivity extends SherlockFragmentActivity {//TabSwipeActivity {

	//private PlayerService mService;
	String tag = "MainActivity";
    //TabHost mTabHost;
	private FragmentAdapter mAdapter;
    private ViewPager mPager;
    //TitlePageIndicator mIndicator;
    
    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main); //!!TabSwipeActivity does that for us.!!
       

        Log.d(tag,"onCreate(), setup FragmentAdapter, ViewPageIndicator");
        mAdapter = new FragmentAdapter(getSupportFragmentManager());

        mPager = (ViewPager)findViewById(R.id.pager);
        mPager.setAdapter(mAdapter);
        
        /** TODO
        TitlePageIndicator indicator = (TitlePageIndicator)findViewById(R.id.indicator);
        indicator.setViewPager(mPager);
        indicator.setFooterIndicatorStyle(IndicatorStyle.Triangle);
        mIndicator = indicator; 
        **/
    
    //check for network availability.
    if (isNetworkAvailable() )
    {
    	Log.d(tag,"koble to internet");
    }else { Log.d(tag,"arxidia internet");}
        
}
    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater inflater = getSupportMenuInflater();
        inflater.inflate(R.menu.menu, menu);
        return true;
    }
    
    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.menu_playPause:
            	//getSupportActionBar().setSelectedNavigationItem(0);
                // 
            	// final ToggleButton playPauseButton = (ToggleButton) this.findViewById(R.id.playPauseButton);
            	 //playPauseButton.performClick();
                return true;
                
            case R.id.menu_stop:
            	//
            	final ImageButton stopButton = (ImageButton) this.findViewById(R.id.stopButton);
            	stopButton.performClick();
            	
            	return true;
            default:
                return super.onOptionsItemSelected(item); 
        }
    }

    @Override
    protected void onSaveInstanceState(Bundle outState) {
    	super.onSaveInstanceState(outState);
    	outState.putInt("tab", getSupportActionBar().getSelectedNavigationIndex());
    	}

    @Override
    protected void onDestroy(){
  	
    	Log.d(tag,"onDestroy(): unbind from playerservice, stop it");
    	ListenFragment frag = new ListenFragment();
    	frag.doUnbindService(); //unbind from playerservice
        stopService(new Intent(this, PlayerService.class));
    	super.onDestroy();
    }
    
    /*
     * Get back press work only at second press and notify user to press again to exit.
     */
    private static long back_pressed;

    @Override
    public void onBackPressed()
    {
            if (back_pressed + 2000 > System.currentTimeMillis()) super.onBackPressed();
            else Toast.makeText(getBaseContext(), "Press once again to exit!", Toast.LENGTH_SHORT).show();
            back_pressed = System.currentTimeMillis();
    }
    
    /*
    * @return boolean return true if the application can access the internet
    */    
    public boolean isNetworkAvailable() {
    	   Context context = getApplicationContext();
           ConnectivityManager connectivity;
        //getSystemService may result in NullPointerException, catch it and return.
        try{
    	   connectivity = (ConnectivityManager) context.getSystemService(Context.CONNECTIVITY_SERVICE);
        }catch (NullPointerException e){
    	      Log.d("isNetWorkAvailable()","getSystemService returned null");
            return false;
        }

    	  NetworkInfo[] info = connectivity.getAllNetworkInfo();
        /* for-each loop instead of classic for */
        int i=0;
    	      if (info != null) {
    	      for( i : info.length){
                if(info[i].getState() == NetworkInfo.State.CONNECTED){
                    return true;
                }
              }
              }
    	      /*   for (int i = 0; i < info.length; i++) {
    	            if (info[i].getState() == NetworkInfo.State.CONNECTED) {
    	               return true;
    	            }
    	         }
    	      }*/
        return false;
   	}
}



    
