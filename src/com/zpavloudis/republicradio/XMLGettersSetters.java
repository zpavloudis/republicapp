package com.zpavloudis.republicradio;

import java.util.ArrayList;

import android.util.Log;

/**
 *  This class contains all getter and setter methods to set and retrieve data.
 *  
 **/
public class XMLGettersSetters {

	private ArrayList<String> name = new ArrayList<String>();
	private ArrayList<String> description = new ArrayList<String>();
	private ArrayList<String> image = new ArrayList<String>();
	private ArrayList<String> time = new ArrayList<String>();
			
	public ArrayList<String> getName() {
		return name;
	}

	public void setName(String name) {
		this.name.add(name);
		//Log.d("This is the name:", name);
	}

	public ArrayList<String> getDesc() {
		return description;
	}

	public void setDesc(String description) {
		this.description.add(description);
		//Log.i("This is the description:", description);
	}

	public ArrayList<String> getImg() {
		return image;
	}

	public void setImg(String image) {
		this.image.add(image);
		//Log.i("This is the image:", image);
	}

	public ArrayList<String> getTime() {
		return time;
	}

	public void setTime(String time) {
		this.time.add(time);
		//Log.i("This is the time:", time);
	}
}
