package com.zpavloudis.republicradio;

import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;


public class FragmentAdapter extends FragmentPagerAdapter {
	
    protected static final String[] CONTENT = new String[] { "Listen", "Program" };

//    private int mCount = CONTENT.length;
private int mCount=1;

    public FragmentAdapter(FragmentManager fm) {
        super(fm);
    }

    @Override
    public Fragment getItem(int position) {
        switch(position) {
        case 0:
        	return ListenFragment.newInstance("Listen");
        case 1:
        	return ProgramFragment.newInstance(CONTENT[1]);
        default:
        	return ListenFragment.newInstance("Listen"); 
        }
    }

    @Override
    public int getCount() {
        return mCount;
    }

    @Override
    public CharSequence getPageTitle(int position) {
      return FragmentAdapter.CONTENT[position % CONTENT.length];
    }

    public void setCount(int count) {
        if (count > 0 && count <= 10) {
            mCount = count;
            notifyDataSetChanged();
        }
    }
}