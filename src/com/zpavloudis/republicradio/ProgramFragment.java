package com.zpavloudis.republicradio;

import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v4.app.ListFragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ProgressBar;
import android.widget.SimpleAdapter;
import org.xml.sax.InputSource;
import org.xml.sax.XMLReader;

import javax.xml.parsers.SAXParser;
import javax.xml.parsers.SAXParserFactory;
import java.net.URL;
import java.util.ArrayList;
import java.util.HashMap;

public class ProgramFragment extends ListFragment{
    private static final String KEY_CONTENT = "Fragment:Program";
    
    public static ProgramFragment newInstance(String content) {
        ProgramFragment fragment = new ProgramFragment();

        fragment.mContent = content;

        return fragment;
    }

    private String mContent = "???";
    ArrayList<HashMap<String,String>> producers = new ArrayList<HashMap<String,String>>();

    // XML node keys
    static final String KEY_ITEM = "item"; // parent node
    static final String KEY_ID = "id";
    static final String KEY_NAME = "name";
    static final String KEY_DESC = "description";
    static final String KEY_IMG = "image";
    static final String KEY_TIME = "time";
 
    private ProgressBar listProgress;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        if ((savedInstanceState != null) && savedInstanceState.containsKey(KEY_CONTENT)) {
            mContent = savedInstanceState.getString(KEY_CONTENT);
        }

    }
    


		@Override
            public View onCreateView(LayoutInflater inflater, ViewGroup container,
                                     Bundle savedInstanceState) {

                // Inflate the layout for this fragment
                return inflater.inflate(R.layout.program_page, container, false);

           }

			//execute the async task on activity finished being made
		 @Override
		  public void onActivityCreated(Bundle savedInstanceState) {
			 	super.onActivityCreated(savedInstanceState);
			 	
		    	listProgress = (ProgressBar)getActivity().findViewById(R.id.listProgressBar);
		        DownloadTask task = new DownloadTask();
		    	listProgress.setVisibility(ProgressBar.VISIBLE);
		        task.execute();
		        listProgress.setVisibility(ProgressBar.GONE);
		       
		}
           
            public static final String EXTRA_TITLE = "title";
            public static Bundle createBundle( String title ) {
                Bundle bundle = new Bundle();
                bundle.putString( EXTRA_TITLE, title );
                return bundle;
            }
            
            //producer object holds values for Map
            public class ProducerInfo {
            	   private String name;
            	   private String description;
            	   private String image;
            	   private String time;
            	   
            	   // constructor, getters and setters
            	   public ProducerInfo(String nm, String desc, String img, String tm)
            	   {
            		   name = nm;
            		   description = desc;
            		   image = img;
            		   time = tm;
            		   
            	   }
            	}
          
            /**
             * Asynchronous Task that handles parsing republic's program XML file
             * and populating the results into the listView
             *
             */
            private class DownloadTask extends AsyncTask<Void, String, Void> {
            	
                private XMLGettersSetters data;

                @Override
                protected Void doInBackground(Void... urls) {

                	try {
            			
            			/**
            			 * Create a new instance of the SAX parser
            			 **/
            			SAXParserFactory saxPF = SAXParserFactory.newInstance();
            			SAXParser saxP = saxPF.newSAXParser();
            			XMLReader xmlR = saxP.getXMLReader();

            			
            			URL url = new URL("http://itsuite.it.brighton.ac.uk/zp25/republic.xml"); // URL of the XML
            			
            			/** 
            			 * Create the Handler to handle each of the XML tags. 
            			 **/
            			XMLHandler myXMLHandler = new XMLHandler();
            			xmlR.setContentHandler(myXMLHandler);
            			xmlR.parse(new InputSource(url.openStream()));
            			
            		} catch (Exception e) {
            			System.out.println(e);
            		}
                	data = XMLHandler.data;
         		   //looping through all item nodes <PRODUCER>
            	    for(int i=0; i < data.getName().size(); i++){
            	        //creating new HashMap
            	        //HashMap<String, ProducerInfo> map = new HashMap<String, ProducerInfo>();
            	    	HashMap<String,String> map = new HashMap<String, String>();
            	    	        	    	
            	        //adding each child node to Hashmap map => value
            	        //map.put(KEY_NAME,new ProducerInfo(data.getName().get(i), data.getDesc().get(i),
            	        //		data.getImg().get(i), data.getTime().get(i)));

            	    	map.put(KEY_NAME,data.getName().get(i));
            	    	map.put(KEY_DESC,data.getDesc().get(i));
            	    	map.put(KEY_IMG, data.getImg().get(i));
            	    	map.put(KEY_TIME, data.getTime().get(i));
            	    	
            	        producers.add(map);
            	    }
					return null;
                }

                //populate our listview
                @Override
                protected void onPostExecute(Void result) {
                	
    			 	SimpleAdapter adapter;
    			    String[] source = new String[]{KEY_NAME, KEY_DESC};//,KEY_IMG,KEY_TIME};
    			    //int[] target = new int[]{R.id.mProducerName, R.id.mProducerDesc};//, R.id.mProducerImg, R.id.mProducerTime};
    			    int[] target = new int[]{android.R.id.text1, android.R.id.text2};
    			    //View listView = getActivity().findViewById(R.id.listView1);
    			    
    			    adapter = new SimpleAdapter(getActivity(), producers, android.R.layout.simple_list_item_2, source, target);
    			    setListAdapter(adapter);                
    			    
                }
              }
} 		
