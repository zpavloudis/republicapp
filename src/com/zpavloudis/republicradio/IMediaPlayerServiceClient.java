/**
 * @author rootlicker http://speakingcode.com
 */

package com.zpavloudis.republicradio;

public interface IMediaPlayerServiceClient {
 
	/**
     * A callback made by a MediaPlayerService onto its clients to indicate that a player is initialising.
     * @param message A message to propagate to the client
     */
    public void onInitialisePlayerStart(String message);
 
    /**
     * A callback made by a MediaPlayerService onto its clients to indicate that a player was successfully initialised.
     */
    public void onInitialisePlayerSuccess();
 
    /**
     *  A callback made by a MediaPlayerService onto its clients to indicate that a player encountered an error.
     */
    public void onError();    
}