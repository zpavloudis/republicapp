package com.zpavloudis.republicradio;

import java.io.IOException;

import android.app.Notification;
import android.support.v4.app.NotificationCompat;
import android.support.v4.app.TaskStackBuilder;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.app.ProgressDialog;
import android.app.Service;
import android.content.Intent;
import android.media.MediaPlayer;
import android.media.AudioManager;
import android.media.MediaPlayer;
import android.media.MediaPlayer.OnBufferingUpdateListener;
import android.media.MediaPlayer.OnErrorListener;
import android.media.MediaPlayer.OnInfoListener;
import android.media.MediaPlayer.OnPreparedListener;
import android.os.Binder;
import android.os.IBinder;
import android.util.Log;
import android.widget.Toast;

import com.zpavloudis.republicradio.MainActivity;
import com.zpavloudis.republicradio.StatefulMediaPlayer.MPStates;

public class PlayerService extends Service implements  OnBufferingUpdateListener, OnInfoListener, 
	OnPreparedListener, OnErrorListener, AudioManager.OnAudioFocusChangeListener {

	static final String tag = "PlayerService";
    static final String ACTION_PLAY = "PLAY";
    private static String mUrl = "http://streamx2.greekradios.gr:8000/";
 
    NotificationManager mNotificationManager;
    Notification.Builder mNotificationBuilder = null;
    Notification mNotification = null;
    final int NOTIFICATION_ID = 1;
    
    private StatefulMediaPlayer mMediaPlayer            = new StatefulMediaPlayer();
    private final Binder mBinder                        = new MediaPlayerBinder();
    private IMediaPlayerServiceClient mClient;
 
    /**
     * A class for clients binding to this service. The client will be passed an object of this class
     * via its onServiceConnected(ComponentName, IBinder) callback.
     */
    public class MediaPlayerBinder extends Binder {
        /**
         * Returns the instance of this service for a client to make method calls on it.
         * @return the instance of this service.
         */
        public PlayerService getService() {
            return PlayerService.this;
        }
 
    }
 
    /**
     * Returns the contained StatefulMediaPlayer
     * @return
     */
    public StatefulMediaPlayer getMediaPlayer() {
        return mMediaPlayer;
    }
    
    /**
     * Initializes a StatefulMediaPlayer for streaming playback of the provided stream url
     * @param mUrl The URL of the stream to play.
     */
    public void initialisePlayer() {
    	
    	mClient.onInitialisePlayerStart("Connecting...");
        mMediaPlayer = new StatefulMediaPlayer();
        mMediaPlayer.setAudioStreamType(AudioManager.STREAM_MUSIC);
        try {
            mMediaPlayer.setDataSource(mUrl);
        }
        catch (Exception e) {
            Log.e("MediaPlayerService", "error setting data source");
            mMediaPlayer.setState(MPStates.ERROR);
        }
        mMediaPlayer.setOnBufferingUpdateListener(this);
        mMediaPlayer.setOnInfoListener(this);
        mMediaPlayer.setOnPreparedListener(this);
        mMediaPlayer.prepareAsync();
    }
       
    @Override
    public IBinder onBind(Intent arg0) {
    	return mBinder;
    }

    
    @Override
    public void onCreate() {
        mNotificationManager = (NotificationManager) getSystemService(NOTIFICATION_SERVICE);
    }


    @Override
    public void onDestroy() {
    	mNotificationManager.cancelAll();
    	
        if (mMediaPlayer != null) {
        	mMediaPlayer.reset(); // <- might fix 
        						// 04-03 15:38:14.583: W/MediaPlayer(3038): mediaplayer went away with unhandled events

            mMediaPlayer.release();
            mMediaPlayer = null;

        }
    }

    @Override
    public int onStartCommand(Intent intent, int flags, int startId) {
        return START_STICKY;
    }
     
    @Override
    public void onBufferingUpdate(MediaPlayer player, int percent) {
 
    }
 
    @Override
    public boolean onError(MediaPlayer player, int what, int extra) {
        mMediaPlayer.reset();
       	mMediaPlayer.release(); // <- might fix 
		// 04-03 15:38:14.583: W/MediaPlayer(3038): mediaplayer went away with unhandled events

        mClient.onError();
        return true;
    }
 
    @Override
    public boolean onInfo(MediaPlayer mp, int what, int extra) {
        return false;
    }
       
    /** Called when MediaPlayer is ready */
    @Override
    public void onPrepared(MediaPlayer player) {
    	Log.d(tag,"Done. Playing..");
        mClient.onInitialisePlayerSuccess();
    	mMediaPlayer.start();
    	setUpAsForeground("Playing",R.drawable.play);
    	  
    }

    /** overrides AudioManager method, handles audio focus states **/
    @Override
    public void onAudioFocusChange(int focusChange) {
        switch (focusChange) {
            case AudioManager.AUDIOFOCUS_GAIN:
                // resume playback
                if (mMediaPlayer == null) initialisePlayer();
                else if (!mMediaPlayer.isPlaying()) mMediaPlayer.start();
                mMediaPlayer.setVolume(1.0f, 1.0f);
                break;

            case AudioManager.AUDIOFOCUS_LOSS:
                // Lost focus for an unbounded amount of time: stop playback and release media player
                if (mMediaPlayer.isPlaying()) mMediaPlayer.stop();
                mMediaPlayer.release();
                mMediaPlayer = null;
                break;

            case AudioManager.AUDIOFOCUS_LOSS_TRANSIENT:
                // Lost focus for a short time, but we have to stop
                // playback. We don't release the media player because playback
                // is likely to resume
                if (mMediaPlayer.isPlaying()) mMediaPlayer.pause();
                break;

            case AudioManager.AUDIOFOCUS_LOSS_TRANSIENT_CAN_DUCK:
                // Lost focus for a short time, but it's ok to keep playing
                // at an attenuated level
                if (mMediaPlayer.isPlaying()) mMediaPlayer.setVolume(0.1f, 0.1f);
                break;
        }
    }
    
    public void play() {
    		mMediaPlayer.start();
    		setUpAsForeground("playing",R.drawable.play);
    }

    public void pause() {
    	 Log.d(tag,"pause() called");
            mMediaPlayer.pause();
          	setUpAsForeground("paused",R.drawable.pause);
        }
    
    public void stop() {
    
    		mNotificationManager.cancelAll();
    		mMediaPlayer.stop();
    		mMediaPlayer.release();	
    		//not sure bout this one:
    		mMediaPlayer = null;
  
    }
    
    public void reset() {
        mMediaPlayer.reset();
    }
    
    /**
     * Sets the client using this service.
     * @param client The client of this service, which implements the IMediaPlayerServiceClient interface
     */
    public void setClient(IMediaPlayerServiceClient client) {
        this.mClient = client;
    }
    
    /**
     * Configures service as a foreground service. A foreground service is a service that's doing something the user is
     * actively aware of (such as playing music), and must appear to the user as a notification. That's why we create
     * the notification here.
     */
    void setUpAsForeground(String text, int icon) {
    	
        Intent notificationIntent = new Intent(this, MainActivity.class);
        TaskStackBuilder stackBuilder = TaskStackBuilder.create(this);
        // Adds the back stack
        stackBuilder.addParentStack(MainActivity.class);
        // Adds the Intent to the top of the stack
        stackBuilder.addNextIntent(notificationIntent);
        // Gets a PendingIntent containing the entire back stack
        PendingIntent pi =
                stackBuilder.getPendingIntent(0, PendingIntent.FLAG_UPDATE_CURRENT);
        
        
        NotificationCompat.Builder mNotificationBuilder = new NotificationCompat.Builder(this)
        .setContentTitle("Republic 100,3")
        .setContentText(text)
        .setContentIntent(pi)
        .setSmallIcon(icon);
        mNotification = mNotificationBuilder.build();
        
              
        
        mNotificationManager = 
  (NotificationManager) getSystemService(NOTIFICATION_SERVICE);
        mNotificationManager.notify(0, mNotification); 
        
        
    }
}